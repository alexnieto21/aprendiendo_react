import React from 'react';

export default class Formulario1 extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            nombre: 'Alex Nieto',
            email: 'alexnieto@gmail.com',
            isActive: true
        }

        // this.inputNombre = React.createRef();
    }

    gestionEnvio = (e) => {
        e.preventDefault();
        // const nombre = this.inputNombre.current.value;
        // const email = document.getElementById("email").value;
        console.log(this.state.nombre, this.state.email);
    }

    // handleInputChange(evento) {
    //     const target = evento.target;
    //     const value = (target.type === 'checkbox') ? target.checked : target.value;
    //     const name = target.name;
    //     this.setState({
    //         [name]: value
    //     });
    // }

    render() {

        //deconstruyo las variables del state para que no deba de escribir this.state...
        const {nombre, email, isActive} = this.state;

        return (
            <div className="container">
                <h4>Formulario</h4>
                <h5>{this.props.children}</h5>
                <form>
                    <div className="form-group">
                        <label htmlFor="name">Nombre</label>
                        <input type="text" 
                                className="form-control" 
                                id="name" 
                                value={nombre} 
                                onChange={e => this.setState({nombre: e.target.value})}
                                // ref={this.inputNombre} 
                                placeholder="Introduce el nombre" />
                    </div>
                    <div className="form-group">
                        <label htmlFor="email">Correo electronico</label>
                        <input type="email" 
                                className="form-control" 
                                id="email" 
                                value={email} 
                                onChange={e => this.setState({email: e.target.value})}
                                placeholder="Introduce el correo electronico"/>
                    </div>
                    <button type="submit" className="btn btn-primary" onClick={this.gestionEnvio}>Enviar</button>
                </form>
            </div>
        );
    }

}