import React from 'react';

export default class Formulario2 extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            form: true,
            text: ''
        }
    }

    formulario = () => {
        return (
            <>
                <input type="text" onChange={e => this.setState({ text: e.target.value })} />
                <input type="button" value="Enviar" onClick={() => this.setState({ form: !this.state.form })} />
            </>
        );
    }

    value = () => {
        return (
            <>
                <p>{this.state.text}</p>
                <button onClick={() => this.setState({ form: !this.state.form })}>Limpiar</button>
            </>
        );
    }

    render() {
        return (
            <div>
                {this.state.form ? this.formulario() : this.value()}
            </div> 
        );
    }

}