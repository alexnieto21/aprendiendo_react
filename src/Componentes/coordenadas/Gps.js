import React from 'react';

export default class Gps extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            latitude: null,
            longitude: null,
            mostrarCoords: false
        }
    }

    mostrar = () => {
        this.setState({ mostrarCoords: true })
        this.leer();
    }

    cerrar = () => {
        clearTimeout(this.timeSimulator);
        navigator.geolocation.clearWatch(this.geo);
        this.setState({
            latitude: null,
            longitude: null,
            mostrarCoords: false
        })
    }

    leer = () => {
        this.geo = navigator.geolocation.watchPosition((position) => {
            let coord = position.coords;
            this.timeSimulator = setTimeout(() => {
                this.setState({
                    latitude: coord.latitude,
                    longitude: coord.longitude
                });
            }, 2000);
        });
    }

    coordenadas = () => {
        return (
            <div>
                <h3>Latitud: {this.state.latitude}</h3>
                <h3>Longitud: {this.state.longitude}</h3>
            </div>
        );
    }

    render() {

        return (
            <>
                {
                    this.state.mostrarCoords
                        ? (this.state.latitude !== null) ? this.coordenadas() : "CARGANDO..."
                        : null
                }
                <div className="mt-3">
                    <button onClick={this.mostrar}>Leer</button>
                    <button onClick={this.cerrar}>Cerrar</button>
                </div>
            </>
        );
    }

}