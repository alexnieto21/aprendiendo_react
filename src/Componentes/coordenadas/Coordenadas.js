import React from 'react';
import Formulario2 from './Formulario2';
import Gps from './Gps';

class Coordenadas extends React.Component{

    constructor(props){
        super(props);

        this.state = {
            verFormulario: false,
            verGps: false
        }
    }

    formulario = () => this.setState({verFormulario: true, verGps: false});
    gps = () => this.setState({verGps: true, verFormulario: false});

    render(){
        return(
            <div className="container text-center">
                <div>
                    <button onClick={this.formulario}>Formulario</button>
                    <button onClick={this.gps}>Coordenadas</button>
                </div>
                {this.state.verFormulario? <Formulario2 /> : null}
                {this.state.verGps? <Gps /> : null}
            </div>
        );
    }

}

export default Coordenadas;