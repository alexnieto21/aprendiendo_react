import React from 'react';

class LineaInf extends React.Component{
    render(){
        return (
            <p>
                Hay {this.props.element}
                &nbsp; plazas a las &nbsp;
                {this.props.horaE} h.
            </p>
        );
    }
}

class Listas extends React.Component{

    render(){
        const horas =  ['7:00', '8:10', '9:30', '10:00', '11:10', '12:00'];
        const numeros = [3, 6, 9, 2, 4, 8];

        let valor=[];

        numeros.forEach((element, index) => {
            valor.push(<LineaInf key={index} element={element} horaE={horas[index]} />)
        })

        return(
            <div>
                <h2>Las listas</h2>
                {numeros.map(num => num*2).join(',')}
                <p>
                    horas: 
                    {horas.map((h, i) => {
                        return <span key={i}> {h}, </span>
                    })}
                </p>
                <h2>La disponibilidad</h2>
                {valor}
            </div>
        );

    }

}

export default Listas;