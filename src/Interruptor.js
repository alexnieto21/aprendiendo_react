import React from 'react';
import './Interruptor.css'

class Interruptor extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            on: this.props.estadoInicial
        }

        this.pulsar = this.pulsar.bind(this);
    }

    pulsar(){
        this.setState({
            on: !this.state.on
        })
    }

    render(){
        let claseIcono = (this.state.on)
        ? "fas fa-toggle-on"
        : "fas fa-toggle-off";

        let claseDiv = (this.state.on)
        ? "interruptor on"
        : "interruptor off";

        return(
                <div onClick={this.pulsar} className={claseDiv}>
                    <i className={claseIcono} aria-hidden="true"></i>
                    <span>{this.props.texto}</span>
                </div>
        );
    }

}

export default Interruptor;