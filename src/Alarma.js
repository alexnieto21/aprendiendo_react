import React from 'react';

class Alarma extends React.Component{
    estado = this.props.isActive? 'Si' : 'No';
    objeto = {
        nombre: this.props.elObjeto.nombre,
        apellido: this.props.elObjeto.apellido,
        trabajo: this.props.elObjeto.trabajo
    }
    render(){
        return(
            <>  
                <p>El estado es: {this.estado}</p>
                <p>El mensaje es: {this.props.mensaje}</p>
                <p>El valor es: {this.props.valor}</p>
                <p>La lista es: {this.props.lista.join(', ')}</p>
                <p>El nombre es: {this.objeto.nombre}</p>
                <p>El apellido es: {this.objeto.apellido}</p>
                <p>El trabajo es: {this.objeto.trabajo}</p>
            </>
        );
    }
}

Alarma.defaultProps = {
    valor: 0,
    mensaje: 'Mensaje por defecto',
    lista: []
}

export default Alarma;
