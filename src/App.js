import React from 'react';
import logo from './logo.svg';
import './App.css';
import Hola2 from './Hola';
import Alarma from './Alarma';
import Listas from './Listas';
import Mouse from './Mouse';
import Formulario1 from './Componentes/formulario/Formulario1';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import Coordenadas from './Componentes/coordenadas/Coordenadas';

function Hola(props){
  return(
    <>
      <h1>{props.mensaje}</h1>
      <Hola2 />
    </>
  ); 
}



function App() {
  return (
    <div className="App">
      <section>

        {/* <Alarma 
            isActive={false} 
            mensaje='Esta es la alarma' 
            // lista = {['Perro', 'Gato', 'Loro', 'Tortuga']}
            elObjeto = {{nombre:'Alex', apellido: 'Nieto', trabajo: 'Programador'}}/> */}

        {/* <Hola mensaje="Hola buenas Puto" />  
        <Hola mensaje="Hola buenas Alex" />   
        <Listas /> */}

        {/* <Mouse titulo="Posiciones del ratón"/> */}
        {/* <Formulario1>
          Este formulario se emplea para pedir los datos de la gente conectada.
        </Formulario1> */}
        <Coordenadas />
      </section>
    </div>
  );
}

export default App;
