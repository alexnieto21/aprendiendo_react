import React from 'react';

class Mouse extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            x: 0,
            y: 0
        }

        this.mouseMove = this.mouseMove.bind(this);
        this.mouseClick = this.mouseClick.bind(this);
    }

    mouseMove(e) {
        e.persist(); //Esto hace que el evento no se pierda en la primera instruccion
        const { clientX, clientY } = e;
        this.setState({
            x: clientX,
            y: clientY
        });
    }

    mouseClick(e) {
        alert("X: " + this.state.x + " Y: " + this.state.y);
    }

    render() {

        let estilos = {
            height: '100vh',
            color: 'white',
            backgroundColor: 'black'
        }

        return (
            <div onMouseMove={this.mouseMove} onClick={this.mouseClick} style={estilos} className="d-flex align-items-center justify-content-center">
                <div>
                    <h2 className="mb-4">{this.props.titulo}</h2>
                    <h4>Posicion X: {this.state.x}</h4>
                    <h4>Posicion X: {this.state.y}</h4>
                </div>
            </div>
        );
    }

}

export default Mouse;