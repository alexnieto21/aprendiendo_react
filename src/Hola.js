import React, {Component} from 'react';
import Interruptor from './Interruptor';

class Hola2 extends Component {

    saluda(event){
        console.log(event);
        alert("Buenos dias");
        let nativo = event.nativeEvent;
        console.log(nativo);
    }

    render(){
        return(
            <>
                <Interruptor estadoInicial texto="Esto es el texto del interruptor" />
                <h2>Y esto desde clases</h2>
                <button onClick={this.saluda}>Saludo</button>
            </>
        ); 
    }
}

export default Hola2;